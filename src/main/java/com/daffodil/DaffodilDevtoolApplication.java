package com.daffodil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 
 * @author yweijian
 * @date 2022年2月28日
 * @version 1.0
 * @description
 */
@EnableDiscoveryClient
@SpringBootApplication
public class DaffodilDevtoolApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DaffodilDevtoolApplication.class);
        application.setWebApplicationType(WebApplicationType.REACTIVE);
        application.addListeners(new ApplicationPidFileWriter());
        application.run(args);
    }
}
