package com.daffodil.devtool.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -代码生成-元数据表
 * @author yweijian
 * @date 2022年2月28日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "gen_metadata")
public class GenMetaData extends BaseEntity<String> {
    private static final long serialVersionUID = -5403855628107752322L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "column_id")
    private String id;
    
    /** 元数据列名称 */
    @Column(name = "column_name")
    @Hql(type = Logical.LIKE)
    private String columnName;

    /** 元数据列描述 */
    @Column(name = "column_comment")
    @Hql(type = Logical.LIKE)
    private String columnComment;

    /** 元数据列类型 */
    @Column(name = "column_type")
    @Hql(type = Logical.EQ)
    private String columnType;

    /** 创建者 */
    @Column(name="create_by")
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @Column(name="create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by")
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @Column(name="update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark")
    @Hql(type = Logical.LIKE)
    private String remark;
    
}
