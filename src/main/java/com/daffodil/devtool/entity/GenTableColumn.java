package com.daffodil.devtool.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.devtool.constant.DevtoolConstant;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -代码生成-库表-字段映射表
 * @author yweijian
 * @date 2022年2月28日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "gen_table_column")
public class GenTableColumn extends BaseEntity<String> {
    private static final long serialVersionUID = -1726023797460382327L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "column_id")
    private String id;
    
    @Column(name = "table_id")
    @Hql(type = Logical.EQ)
    private String tableId;

    /** 列名称 */
    @Column(name = "column_name")
    private String columnName;

    /** 列描述 */
    @Column(name = "column_comment")
    private String columnComment;

    /** 列类型 */
    @Column(name = "column_type")
    private String columnType;

    /** JAVA字段名 */
    @Column(name = "java_field")
    private String javaField;
    
    /** 主键ID生成策略 */
    @Column(name = "column_identifier")
    private String columnIdentifier;

    /** 是否主键（Y是） */
    @Column(name = "is_pk")
    private String isPk;

    /** 是否必填（Y是） */
    @Column(name = "is_required")
    private String isRequired;

    /** 是否为插入字段（Y是） */
    @Column(name = "is_insert")
    private String isInsert;

    /** 是否编辑字段（Y是） */
    @Column(name = "is_edit")
    private String isEdit;

    /** 是否列表字段（Y是） */
    @Column(name = "is_list")
    private String isList;

    /** 是否查询字段（Y是） */
    @Column(name = "is_query")
    private String isQuery;

    /** 查询方式 LIKE("like"), EQ("="), NEQ("!="), LEQ("<="), LT("<"), REQ(">="), RT(">") */
    @Column(name = "query_type")
    private String queryType;

    /** 显示类型（input文本框、textarea文本域、select下拉框、checkbox复选框、radio单选框、datetime日期控件） */
    @Column(name = "html_type")
    private String htmlType;

    /** 字典类型 */
    @Column(name = "dict_type")
    private String dictType;

    /** 排序 */    
    @Column(name = "sort")
    private Integer sort;
    
    /** 创建者 */
    @Column(name="create_by")
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @Column(name="create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by")
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @Column(name="update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark")
    @Hql(type = Logical.LIKE)
    private String remark;
    
    @Transient
    public boolean isPk() {
        return isPk(this.isPk);
    }

    public static boolean isPk(String isPk) {
        return isPk != null && StringUtils.equals(DevtoolConstant.YES, isPk);
    }

    @Transient
    public boolean isRequired() {
        return isRequired(this.isRequired);
    }

    public static boolean isRequired(String isRequired) {
        return isRequired != null && StringUtils.equals(DevtoolConstant.YES, isRequired);
    }

    @Transient
    public boolean isInsert() {
        return isInsert(this.isInsert);
    }

    public static boolean isInsert(String isInsert) {
        return isInsert != null && StringUtils.equals(DevtoolConstant.YES, isInsert);
    }

    @Transient
    public boolean isEdit() {
        return isInsert(this.isEdit);
    }

    public static boolean isEdit(String isEdit) {
        return isEdit != null && StringUtils.equals(DevtoolConstant.YES, isEdit);
    }

    @Transient
    public boolean isList() {
        return isList(this.isList);
    }

    public static boolean isList(String isList) {
        return isList != null && StringUtils.equals(DevtoolConstant.YES, isList);
    }

    @Transient
    public boolean isQuery() {
        return isQuery(this.isQuery);
    }

    public static boolean isQuery(String isQuery) {
        return isQuery != null && StringUtils.equals(DevtoolConstant.YES, isQuery);
    }

    public String readConverterExp() {
        String remarks = StringUtils.substringBetween(this.columnComment, "（", "）");
        StringBuffer sb = new StringBuffer();
        if (StringUtils.isNotEmpty(remarks)) {
            for (String value : remarks.split(" ")) {
                if (StringUtils.isNotEmpty(value)) {
                    Object startStr = value.subSequence(0, 1);
                    String endStr = value.substring(1);
                    sb.append("").append(startStr).append("=").append(endStr).append(",");
                }
            }
            return sb.deleteCharAt(sb.length() - 1).toString();
        } else {
            return this.columnComment;
        }
    }
}