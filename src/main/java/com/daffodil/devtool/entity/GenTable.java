package com.daffodil.devtool.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.daffodil.devtool.constant.DevtoolConstant;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -代码生成数据库表
 * @author yweijian
 * @date 2022年2月28日
 * @version 1.0
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "gen_table")
public class GenTable extends BaseEntity<String> {

    private static final long serialVersionUID = 7898834898363662452L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "table_id")
    private String id;
    
    /** 表名称 */
    @NotBlank(message = "表名称不能为空")
    @Column(name = "table_name")
    @Hql(type = Logical.LIKE)
    private String tableName;

    /** 表描述 */
    @NotBlank(message = "表描述不能为空")
    @Column(name = "table_comment")
    @Hql(type = Logical.LIKE)
    private String tableComment;

    /** 实体类名称(首字母大写) */
    @NotBlank(message = "实体类名称不能为空")
    @Column(name = "class_name")
    @Hql(type = Logical.LIKE)
    private String className;
    
    /** 使用的模板代码仓库 */
    @Column(name = "tpl_codehouse")
    @Hql(type = Logical.EQ)
    private String tplCodehouse;

    /** 使用的模板（crud数据列表 tree树形列表） */
    @Column(name = "tpl_category")
    @Hql(type = Logical.EQ)
    private String tplCategory;
    
    /** 使用的运行容器（webflux响应式 webmvc非响应式） */
    @Column(name = "tpl_container")
    @Hql(type = Logical.EQ)
    private String tplContainer;

    /** 生成包路径 */
    @Column(name = "package_name")
    private String packageName;

    /** 生成模块名 */
    @Column(name = "module_name")
    private String moduleName;

    /** 生成业务名 */
    @Column(name = "business_name")
    private String businessName;

    /** 功能上下文 */
    @Column(name = "function_api")
    private String functionApi;
    
    /** 生成功能名 */
    @Column(name = "function_name")
    private String functionName;

    /** 生成作者 */
    @NotBlank(message = "作者不能为空")
    @Column(name = "function_author")
    private String functionAuthor;

    /** 树编码字段 */
    @Column(name = "tree_code")
    private String treeCode;

    /** 树父编码字段 */
    @Column(name = "tree_parent_code")
    private String treeParentCode;

    /** 树名称字段 */
    @Column(name = "tree_name")
    private String treeName;
    
    /** 创建者 */
    @Column(name="create_by")
    @Hql(type = Logical.LIKE)
    private String createBy;

    /** 创建时间 */
    @Column(name="create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    @Column(name="update_by")
    @Hql(type = Logical.LIKE)
    private String updateBy;

    /** 更新时间 */
    @Column(name="update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 备注 */
    @Column(name="remark")
    @Hql(type = Logical.LIKE)
    private String remark;
    
    /** 主键信息 */
    @Transient
    private GenTableColumn pkColumn;

    /** 表列信息 */
    @Transient
    private List<GenTableColumn> columns;
    
    @Transient
    public boolean isTree() {
        return isTree(this.tplCategory);
    }

    public static boolean isTree(String tplCategory) {
        return tplCategory != null && StringUtils.equals(DevtoolConstant.TPL_TREE, tplCategory);
    }

    @Transient
    public boolean isCrud() {
        return isCrud(this.tplCategory);
    }

    public static boolean isCrud(String tplCategory) {
        return tplCategory != null && StringUtils.equals(DevtoolConstant.TPL_CRUD, tplCategory);
    }
    
    @Transient
    public boolean isWebflux() {
        return isWebflux(this.tplContainer);
    }

    public static boolean isWebflux(String tplContainer) {
        return tplContainer != null && StringUtils.equals(DevtoolConstant.TPL_WEBFLUX, tplContainer);
    }
    
    @Transient
    public boolean isWebmvc() {
        return isWebmvc(this.tplContainer);
    }

    public static boolean isWebmvc(String tplContainer) {
        return tplContainer != null && StringUtils.equals(DevtoolConstant.TPL_WEBMVC, tplContainer);
    }
}