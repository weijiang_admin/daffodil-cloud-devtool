package com.daffodil.devtool.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.*;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * -生成代码模板对象
 * @author yweijian
 * @date 2023-08-07
 * @version 2.0.0
 * @copyright Copyright 2020-2023 www.daffodilcloud.com.cn
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "gen_template")
public class GenTemplate extends BaseEntity<String> {

    private static final long serialVersionUID = 1L;
    
    /** 主键编号 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;
    
    /** 模板名称 */
    @Column(name = "name")
    @NotBlank(message = "模板名称不能为空")
    @Hql(type = Logical.LIKE)
    private String name;
    
    /** 命名空间 */
    @Column(name = "namespace")
    @Hql(type = Logical.LIKE)
    private String namespace;
    
    /** 生成路径 */
    @Column(name = "path")
    private String path;
    
    /** 模板内容 */
    @Column(name = "content")
    private String content;
    
    /** 备注 */
    @Column(name = "remark")
    @Hql(type = Logical.LIKE)
    private String remark;
    
    /** 创建者 */
    @Column(name = "create_by")
    private String createBy;
    
    /** 创建时间 */
    @Column(name = "create_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    
    /** 更新者 */
    @Column(name = "update_by")
    private String updateBy;
    
    /** 更新时间 */
    @Column(name = "update_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    

}
