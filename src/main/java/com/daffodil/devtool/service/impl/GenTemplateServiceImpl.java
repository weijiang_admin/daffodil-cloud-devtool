package com.daffodil.devtool.service.impl;

import org.springframework.stereotype.Service;

import com.daffodil.core.service.impl.BaseEntityServiceImpl;
import com.daffodil.devtool.entity.GenTemplate;
import com.daffodil.devtool.service.IGenTemplateService;

/**
 * -生成代码模板Service接口业务实现层
 * @author yweijian
 * @date 2023-08-07
 * @version 2.0.0
 * @copyright Copyright 2020-2023 www.daffodilcloud.com.cn
 * @description
 */
@Service
public class GenTemplateServiceImpl extends BaseEntityServiceImpl<String, GenTemplate> implements IGenTemplateService {

}
