package com.daffodil.devtool.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.devtool.entity.GenTableColumn;


/**
 * 业务字段 服务层
 * @author yweijian
 * @date 2022年2月28日
 * @version 1.0
 * @description
 */
public interface IGenTableColumnService {
    
    /**
     * 查询业务字段列表
     * @param query 业务字段信息
     * @return 业务字段集合
     */
    public List<GenTableColumn> selectGenTableColumnList(Query<GenTableColumn> query);
    

    /**
     * 根据元数据ids批量新增业务字段
     * @param columnIds 元数据的ID
     * @param tableId
     */
    public void insertGenTableColumn(List<GenTableColumn> tableColumns);


}
