package com.daffodil.devtool.service;

import com.daffodil.core.service.IBaseEntityService;
import com.daffodil.devtool.entity.GenTemplate;

/**
 * -生成代码模板Service接口
 * @author yweijian
 * @date 2023-08-07
 * @version 2.0.0
 * @copyright Copyright 2020-2023 www.daffodilcloud.com.cn
 * @description
 */
public interface IGenTemplateService extends IBaseEntityService<String, GenTemplate> {

}
