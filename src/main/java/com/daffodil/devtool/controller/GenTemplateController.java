package com.daffodil.devtool.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.framework.controller.ReactiveBaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.devtool.constant.DevtoolConstant;
import com.daffodil.devtool.entity.GenTemplate;
import com.daffodil.devtool.service.IGenTemplateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;
import reactor.core.publisher.Mono;

/**
 * -生成代码模板控制层
 * @author yweijian
 * @date 2023-08-07
 * @version 2.0.0
 * @copyright Copyright 2020-2023 www.daffodilcloud.com.cn
 * @description
 */
@Api(value = "生成代码模板管理", tags = "生成代码模板管理")
@RestController
@RequestMapping(DevtoolConstant.API_CONTENT_PATH + "/template")
public class GenTemplateController extends ReactiveBaseController {

    @Autowired
    private IGenTemplateService genTemplateService;

    @ApiOperation("分页查询生成代码模板列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("devtool:template:list")
    @GetMapping("/list")
    public Mono<TableResult> list(GenTemplate genTemplate, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(genTemplate, page, request);
        List<GenTemplate> list = genTemplateService.selectEntityList(query);
        return Mono.just(TableResult.success(list, query));
    }

    @ApiOperation("获取生成代码模板详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("devtool:template:info")
    @GetMapping("/info")
    public Mono<JsonResult> info(@ApiParam(value = "生成代码模板ID") String id, @ApiIgnore ServerHttpRequest request) {
        GenTemplate genTemplate = genTemplateService.selectEntityById(id);
        return Mono.just(JsonResult.success(genTemplate));
    }

    @ApiOperation("新增生成代码模板")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("devtool:template:add")
    @OperLog(title = "生成代码模板管理", type = Business.INSERT)
    @PostMapping("/add")
    public Mono<JsonResult> add(@Validated @RequestBody GenTemplate genTemplate, @ApiIgnore ServerHttpRequest request) {
        genTemplateService.insertEntity(genTemplate);
        return Mono.just(JsonResult.success(genTemplate));
    }

    @ApiOperation("修改生成代码模板")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("devtool:template:edit")
    @OperLog(title = "生成代码模板管理", type = Business.UPDATE)
    @PostMapping("/edit")
    public Mono<JsonResult> edit(@Validated @RequestBody GenTemplate genTemplate, @ApiIgnore ServerHttpRequest request) {
        genTemplateService.updateEntity(genTemplate);
        return Mono.just(JsonResult.success(genTemplate));
    }
    
    @ApiOperation("删除生成代码模板")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("devtool:template:remove")
    @OperLog(title = "生成代码模板管理", type = Business.DELETE)
    @PostMapping("/remove")
    public Mono<JsonResult> remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        genTemplateService.deleteEntityByIds(ids);
        return Mono.just(JsonResult.success());
    }

}
