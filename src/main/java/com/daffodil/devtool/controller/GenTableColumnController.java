package com.daffodil.devtool.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableResult;
import com.daffodil.devtool.constant.DevtoolConstant;
import com.daffodil.devtool.entity.GenTableColumn;
import com.daffodil.devtool.service.IGenTableColumnService;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2022年2月28日
 * @version 1.0
 * @description
 */
@Api(value = "库表字段管理", tags = "库表字段管理")
@RestController
@RequestMapping(DevtoolConstant.API_CONTENT_PATH)
public class GenTableColumnController extends ReactiveBaseController {
    
    @Autowired
    private IGenTableColumnService tableColumnService;

    @ApiOperation("查询库表字段列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("devtool:gencolumn:list")
    @GetMapping("/gencolumn/list")
    public TableResult list(GenTableColumn tableColumn, @ApiIgnore ServerHttpRequest request) {
        initQuery(tableColumn, request);
        List<GenTableColumn> list = tableColumnService.selectGenTableColumnList(query);
        return TableResult.success(list);
    }

    @ApiOperation("保存库表字段")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("devtool:gencolumn:add")
    @OperLog(title = "库表字段管理", type = Business.INSERT)
    @PostMapping("/gencolumn/add")
    public JsonResult add(@RequestBody List<GenTableColumn> tableColumns, @ApiIgnore ServerHttpRequest request) {
        tableColumnService.insertGenTableColumn(tableColumns);
        return JsonResult.success();
    }

}
