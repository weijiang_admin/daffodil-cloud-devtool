package com.daffodil.devtool.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableResult;
import com.daffodil.devtool.constant.DevtoolConstant;
import com.daffodil.devtool.entity.GenMetaData;
import com.daffodil.devtool.service.IGenMetaDataService;
import com.daffodil.framework.annotation.AuthPermission;
import com.daffodil.framework.annotation.OperLog;
import com.daffodil.framework.annotation.OperBusiness.Business;
import com.daffodil.framework.controller.ReactiveBaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 
 * @author yweijian
 * @date 2022年2月28日
 * @version 1.0
 * @description
 */
@Api(value = "元数据管理", tags = "元数据管理")
@RestController
@RequestMapping(DevtoolConstant.API_CONTENT_PATH)
public class GenMetaDataController extends ReactiveBaseController {
    
    @Autowired
    private IGenMetaDataService metaDataService;

    @ApiOperation("分页查询元数据列表")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @SuppressWarnings("unchecked")
    @AuthPermission("devtool:metadata:list")
    @GetMapping("/metadata/list")
    public TableResult list(GenMetaData metaData, Page page, @ApiIgnore ServerHttpRequest request) {
        initQuery(metaData, page, request);
        List<GenMetaData> list = metaDataService.selectGenMetaDataList(query);
        return TableResult.success(list, query);
    }

    @ApiOperation("获取元数据详情")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @GetMapping("/metadata/info")
    public JsonResult info(@ApiParam(value = "元数据ID") String id, @ApiIgnore ServerHttpRequest request) {
        GenMetaData metaData = metaDataService.selectGenMetaDataById(id);
        return JsonResult.success(metaData);
    }
    
    @ApiOperation("新增元数据")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("devtool:metadata:add")
    @OperLog(title = "元数据管理", type = Business.INSERT)
    @PostMapping("/metadata/add")
    public JsonResult add(@Validated @RequestBody GenMetaData metaData, @ApiIgnore ServerHttpRequest request) {
        metaDataService.insertGenMetaData(metaData);
        return JsonResult.success();
    }

    @ApiOperation("修改元数据")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("devtool:metadata:edit")
    @OperLog(title = "元数据管理", type = Business.UPDATE)
    @PostMapping("/metadata/edit")
    public JsonResult edit(@Validated @RequestBody GenMetaData metaData, @ApiIgnore ServerHttpRequest request) {
        metaDataService.updateGenMetaData(metaData);
        return JsonResult.success();
    }

    @ApiOperation("删除元数据")
    @ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, value = "登录授权令牌 Bearer token", paramType = "header", required = true)
    @AuthPermission("devtool:metadata:remove")
    @OperLog(title = "元数据管理", type = Business.DELETE)
    @PostMapping("/metadata/remove")
    public JsonResult remove(@RequestBody String[] ids, @ApiIgnore ServerHttpRequest request) {
        metaDataService.deleteGenMetaDataByIds(ids);
        return JsonResult.success();
    }
}
