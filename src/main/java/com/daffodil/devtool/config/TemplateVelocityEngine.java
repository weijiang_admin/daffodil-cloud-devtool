package com.daffodil.devtool.config;

import java.util.Properties;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Component;

/**
 * 
 * @author yweijian
 * @date 2023年4月25日
 * @version 2.0.0
 * @description
 */
@Component
public class TemplateVelocityEngine extends VelocityEngine {

    public TemplateVelocityEngine() {
        super();
        Properties properties = new Properties();
        //properties.setProperty("resource.loader.file.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        properties.setProperty("resource.loader.file.class", "com.daffodil.devtool.config.TemplateResourceLoader");
        properties.setProperty(Velocity.INPUT_ENCODING, Velocity.ENCODING_DEFAULT);
        this.setProperties(properties);
    }
}
